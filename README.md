# Mongobuild

Mongobuild is a CLI tool intended to help manage MongoDB collections, indexes, schema validations, and views.
It can also generate TypeScript types based on the schema file, so you don't have to write it all by hand.

## Usage

The first step is to make a schema.json file. You have two options:

1. Write the file manually
2. If you already have a database, you can run `mongobuild pull --dir ./schema-dump`.
   This will pull all collection configuration from the database.
   Then you can edit the schema, as needed

When you have a schema file, you can push it to the database with this command:

```sh
mongobuild push -s ./schema.json -o ./mongo.d.ts
```

This does multiple things:

1. Create new collections if they don't exist
2. Update validation schemas for all new/existing collections
3. Create/update views
4. Generates TypeScript types, and outputs them to `./mongo.d.ts`.
   The types are determined by looking at the `validator.$jsonSchema` field on collections

The schema.json file follows this structure:

```ts
type Schema = {
    // name of your database
    databaseName: string;
    // collections in the database
    collections: Collection[];
};

type Collection = {
    // name of your collections
    name: string;
    // (optional) validator for the collection.
    // This is also used for generating TypeScript types.
    // For reference, see https://www.mongodb.com/docs/manual/core/schema-validation/
    validator: object;
    // (optional) creates a view on the specified collection/view
    // The view's aggregation pipeline will be based on this collection
    // For reference, see https://www.mongodb.com/docs/manual/core/views/create-view/#use-db.createcollection---to-create-a-view
    viewOn: string;
    // (optional) the aggregation pipeline for the view
    pipeline: object[];
};
```

### Example

```json
{
    "databaseName": "production",
    "collections": [
        {
            "name": "users",
            "validator": {
                "$jsonSchema": {
                    "bsonType": "object",
                    "required": ["_id", "email", "roles"],
                    "properties": {
                        "_id": {
                            "bsonType": "objectId",
                            "description": "_id must be an ObjectId and is required"
                        },
                        "email": { "bsonType": "string" },
                        "roles": {
                            "bsonType": "array",
                            "items": {
                                "bsonType": "object",
                                // Used as type name when generating TS declaration file
                                "title": "UserRole",
                                "required": ["name", "color"],
                                "properties": {
                                    "name": { "bsonType": "string" },
                                    "color": { "bsonType": "string" }
                                }
                            }
                        }
                    }
                }
            }
        },
        {
            "name": "groups",
            "viewOn": "users",
            "pipeline": [
                {
                    "$group": {
                        "_id": "$roles.color",
                        "count": { "$sum": 1 }
                    }
                }
            ]
        }
    ]
}
```

The following types will be generated after running `mongobuild push -o ./types.d.ts`

```ts
export type UserRole = {
    name: string;
    color: string;
};

export type Users = {
    /**
     * _id must be an ObjectId and is required
     */
    _id: ObjectId;
    email: string;
    roles: UserRole[];
};
```
