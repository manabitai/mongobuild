const Indexes = require("./indexes.js");
const { existsSync, mkdirSync, writeFileSync } = require("fs");

/**
 * @typedef {import("./types.d.ts").Collection} CollectionSchema
 * @typedef {import("./types.d.ts").Schema} Schema
 * @typedef {import("mongodb").IndexDescription} IndexDescription
 * @typedef {import("mongodb").CollectionInfo} CollectionInfo
 * @typedef {import("mongodb").Db} Db
 * @typedef {import("mongodb").MongoClient} MongoClient
 */

/**
 * @param {MongoClient} client
 * @param {Schema} schema
 * @returns {Promise<void>}
 */
async function push(client, schema) {
    console.group(`Pushing schema to ${schema.databaseName}`);

    const collections = await client
        .db(schema.databaseName)
        .listCollections()
        .toArray();

    const db = client.db(schema.databaseName);

    for (const collectionSchema of schema.collections) {
        await pushOne(db, collections, collectionSchema);
    }

    console.groupEnd();
}

/**
 * @param {Db} db
 * @param {CollectionInfo[]} collections
 * @param {CollectionSchema[]} schema
 * @returns {Promise<void>}
 */
async function pushOne(db, collections, schema) {
    console.info(`Pushing schema to ${db.databaseName}.${schema.name}`);

    const { name, viewOn, pipeline, validator, indexes } = schema;
    const opts = {};
    if (viewOn) opts["viewOn"] = viewOn;
    if (pipeline) opts["pipeline"] = pipeline;
    if (validator) opts["validator"] = validator;

    if (collections.some((c) => c.name === name)) {
        await db.command({ collMod: name, ...opts });
    } else {
        await db.createCollection(name, opts);
    }

    if (indexes !== undefined) {
        const collection = db.collection(name);
        Indexes.push(collection, indexes);
    }
}

/**
 * @param {MongoClient} client
 * @returns {Promise<Schema>}
 */
async function pull(client, outDir) {
    if (!existsSync(outDir)) mkdirSync(outDir);

    const res = await client.db("admin").command({ listDatabases: 1 });
    const databases = res.databases.filter(
        (db) => db.name !== "local" && db.name !== "admin",
    );

    for (const database of databases) {
        const databaseSchema = {
            databaseName: database.name,
            collections: [],
        };

        const db = client.db(database.name);
        const collections = await client
            .db(database.name)
            .listCollections()
            .toArray();

        for (const collection of collections) {
            const indexes = await Indexes.pull(db, collection);
            databaseSchema.collections.push({
                name: collection.name,
                indexes,
                ...collection.options,
            });
        }
        writeFileSync(
            `${outDir}/${database.name}.json`,
            JSON.stringify(databaseSchema, null, 4),
        );
    }
}

module.exports = {
    push,
    pull,
};
