#! /usr/bin/env node

/**
 * @typedef {import("./types.d.ts").Schema} Schema
 * @typedef {import("./types.d.ts").Collection} Collection
 * @typedef {import("./types.d.ts").View} View
 */

const dotenv = require("dotenv");
const { readFileSync } = require("fs");
const { program } = require("commander");
const { Collection } = require("mongodb");
const { push, pull } = require("./index.js");

program
    .name("mongobuild")
    .version("0.1.0")
    .command("push")
    .description("push the schema to a MongoDB database")
    .action(handlePush)
    .option("-s, --schema <schemaPath>", "path to schema file")
    .option(
        "-o, --out <outPath>",
        "path to file where to output generated types",
    )
    .option("--url <url>", "MongoDB connection string");

program
    .name("mongobuild")
    .version("0.1.0")
    .command("pull")
    .description("pull the schema from a MongoDB database")
    .action(pull)
    .option(
        "-d, --dir <schema dir>",
        "path to dir where schemas will be dumped",
    )
    .option("--url <url>", "MongoDB connection string");

dotenv.config();
program.parse();

/**
 * @param {{ schema?: string, out?: string; }} options
 */
async function handlePush({ schema: schemaPath, out: outPath, url }) {
    schemaPath ??= "./schema.json";
    const schema = JSON.parse(readFileSync(schemaPath));
    await push({ schema, outPath, url });
}
