import type { IndexDescription } from "mongodb";

export type Schema = {
    databaseName: string;
    collections: Array<Collection | View>;
};

export type Collection = {
    name: string;
    indexes?: IndexDescription[];
    validator?: Validator;
};

export type View = {
    name: string;
    viewOn: string;
    pipeline: Record<string, any>[];
};

export type Validator = {
    $jsonSchema: JsonSchema;
};

export type JsonSchema =
    | ArrayJsonSchema
    | ObjectJsonSchema
    | { bsonType: string | string[] };

export type ArrayJsonSchema = {
    bsonType: "array";
    items?: JsonSchema;
};

export type ObjectJsonSchema = {
    bsonType: "object";
    required?: string[];
    properties: Record<string, JsonSchema>;
};
