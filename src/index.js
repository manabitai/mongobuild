#! /usr/bin/env node

/**
 * @typedef {import("./types.d.ts").Schema} Schema
 * @typedef {import("./types.d.ts").Collection} Collection
 * @typedef {import("./types.d.ts").View} View
 */

const { writeFileSync } = require("fs");
const { MongoClient, Collection } = require("mongodb");
const { TypeGenerator } = require("./generate/generator.js");
const Collections = require("./collections.js");

/**
 * @param {{ schema: Schema, outPath?: string; url?: string }} options
 */
async function push({ schema, outPath, url }) {
    try {
        if (outPath !== undefined) {
            writeFileSync(outPath, generateTypes(schema));
            console.info("wrote types to " + outPath);
        }
        const client = await MongoClient.connect(getConnectionString(url));
        await Collections.push(client, schema);
        await client.close();
    } catch (err) {
        console.error(err);
        process.exit(1);
    }
}

/**
 * @param {{ dir?: string, url?: string }} options
 */
async function pull({ dir: outDir, url }) {
    try {
        outDir ??= "./mongo-schemas";
        const client = await MongoClient.connect(getConnectionString(url));
        await Collections.pull(client, outDir);
        await client.close();
    } catch (err) {
        console.error(err);
        process.exit(1);
    }
}

/**
 * @param {string | undefined} url
 */
function getConnectionString(url) {
    if (url !== undefined) return url;
    if (process.env.DATABASE_URL !== undefined) return process.env.DATABASE_URL;
    throw new Error(
        "missing database connection string, either use the --url option, or set the DATABASE_URL environment variable (also read from .env)",
    );
}

/**
 * @param {Schema} schema
 */
function generateTypes(schema) {
    const generator = new TypeGenerator();
    for (const collection of schema.collections) {
        if (collection.validator?.$jsonSchema === undefined) continue;
        generator.addBsonObjectDefinition(
            collection.name,
            collection.validator.$jsonSchema,
        );
    }
    return generator.generate();
}

module.exports = {
    push,
    pull,
};
