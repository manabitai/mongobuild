/**
 * @typedef {import("mongodb").IndexDescription} IndexDescription
 * @typedef {import("mongodb").Collection} Collection
 * @typedef {import("mongodb").Db} Db
 */

/**
 * @param {Collection} collection
 * @param {IndexDescription[]} index
 * @returns {Promise<void>}
 */
async function push(collection, indexes) {
    const existingIndexes = await collection.listIndexes().toArray();
    for (const existingIndex of existingIndexes) {
        if (existingIndex.name === "_id_") continue;
        let isInSchema = false;
        for (const index of indexes) {
            if (existingIndex.name === index.name) {
                await collection.dropIndex(index.name);
                isInSchema = true;
                break;
            }
        }
        if (!isInSchema) {
            await collection.dropIndex(existingIndex.name);
        }
    }
    await collection.createIndexes(indexes);
}

/**
 * @param {Db} db
 * @param {import("./collections").CollectionInfo} info
 * @returns {Promise<IndexDescription[] | undefined>}
 */
async function pull(db, info) {
    if (info?.type === "view") return undefined;
    const collection = db.collection(info.name);
    const indexes = await collection.listIndexes().toArray();
    return indexes.filter((index) => index.name !== "_id_");
}

module.exports = {
    push,
    pull,
};
