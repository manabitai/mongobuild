/**
 * @example
 * type Name {
 *     fieldName: fieldType[];
 * };
 * @property {string} name name of the type definition
 * @property {InterfaceFieldDef[]} fields fields of the type definition
 */
class TypeDef {
    /**
     * @param {string} name
     * @param {InterfaceFieldDef[]} fields
     */
    constructor(name, fields) {
        this.name = name;
        this.fields = fields;
    }

    /**
     * @returns {string}
     */
    toString() {
        const fields = this.fields.map((field) => indent(4, field)).join("\n");
        return `export type ${this.name} = {\n${fields}\n};`;
    }
}

/**
 * @example
 * fieldName: fieldType[];
 * @property {string} name
 * @property {string} type
 * @property {boolean} isOptional
 * @property {string | null} jsdoc
 */
class InterfaceFieldDef {
    /**
     * @param {string} name
     * @param {string} type
     * @param {boolean} isOptional
     * @param {string | null} jsdoc
     */
    constructor(name, type, isOptional = false, jsdoc = null) {
        this.name = name;
        this.type = type;
        this.isOptional = isOptional;
        this.jsdoc = jsdoc;
    }

    /**
     * @returns {string}
     */
    toString() {
        return `${this.jsdoc ? `${this.jsdoc}\n` : ""}${this.name}${this.isOptional ? "?" : ""}: ${this.type};`;
    }
}

function indent(indentation, node) {
    return node
        .toString()
        .split("\n")
        .map((line) => " ".repeat(indentation) + line)
        .join("\n");
}

module.exports = {
    TypeDef,
    InterfaceFieldDef,
};
