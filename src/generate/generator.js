const { TypeDef, InterfaceFieldDef } = require("./ast.js");

/**
 * @typedef {import("../types.d.ts").ObjectJsonSchema} ObjectJsonSchema
 */

class TypeGenerator {
    /** @type {object[]} */
    #nodes = [];
    /** @type {boolean} */
    #hasObjectId = false;
    /** @type {Map<string, string>} */
    #tMap = new Map();

    /**
     * @param {string} name
     * @param {ObjectJsonSchema} schema
     */
    addBsonObjectDefinition(name, schema) {
        let typeName = this.#stringToTypeName(name);
        const fieldMap = new Map();
        const fields = [];

        for (const key in schema.properties) {
            const item = schema.properties[key];

            const fieldName = item.title ?? key;
            const isOptional = !schema.required?.includes(fieldName);
            const type = this.#bsonTypeToTsType(fieldName, item);
            let jsdoc = null;

            if (item.description) jsdoc = this.#description(item.description);

            const hasoid =
                item.bsonType === "objectId" ||
                (Array.isArray(item.bsonType) &&
                    item.bsonType.includes("objectId"));
            if (hasoid) this.#hasObjectId = true;

            fieldMap.set(fieldName, type);
            fields.push(
                new InterfaceFieldDef(fieldName, type, isOptional, jsdoc),
            );
        }

        this.#tMap.set(name, { name: typeName, fields: fieldMap });
        this.#nodes.push(new TypeDef(typeName, fields));
    }

    /**
     * @returns {string}
     */
    generate() {
        let output = this.#nodes.join("\n\n");
        if (this.#hasObjectId)
            output = 'import { ObjectId } from "mongodb";\n\n' + output;
        return output;
    }

    /**
     * Generate a JSDoc comment for the collection type
     */
    #description(text) {
        const description = text
            .split("\n")
            .map((line) => ` * ${line}`)
            .join("\n");
        return `/**\n${description}\n */`;
    }

    /**
     * @param type {string | string[]}
     * @returns {string}
     */
    #bsonTypeToTsType(name, schema) {
        if (schema.bsonType === "object") {
            this.addBsonObjectDefinition(name, schema);
            return this.#stringToTypeName(name);
        }

        if (schema.bsonType === "array") {
            if (schema.items === undefined) {
                return "unknown[]";
            } else if (schema.items.bsonType === "object") {
                const typeName = schema.items.title ?? name + "Item";
                return this.#bsonTypeToTsType(typeName, schema.items) + "[]";
            } else {
                return this.#simpleTypeToTsType(schema.items.bsonType) + "[]";
            }
        }

        return this.#simpleTypeToTsType(schema.bsonType);
    }

    /**
     * @param type {string | string[]}
     * @returns {string}
     */
    #simpleTypeToTsType(type) {
        if (Array.isArray(type)) {
            const res = [];
            for (const t of type) res.push(this.#simpleTypeToTsType(t));
            return res.join(" | ");
        }

        switch (type) {
            case "date":
                return "Date";
            case "objectId":
                return "ObjectId";
            case "bool":
                return "boolean";
            case "double":
            case "int":
                return "number";
            case "array":
                return "unknown[]";
            default:
                return type;
        }
    }

    /**
     * @example "users" => "Users"
     * @param name {string}
     * @returns {string}
     */
    #stringToTypeName(name) {
        return name[0].toUpperCase() + name.substring(1);
    }
}

module.exports = {
    TypeGenerator,
};
